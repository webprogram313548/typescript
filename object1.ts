// const car: {type: string, model: string, year: number} = {
//     type: "Toyota",
//     model: "carolla",
//     year: 2009
// };

// console.log(car);


//don't have console
// const car = {
//     type: "Toyota",
//     model: "carolla",
//     year: 2009
// };


// optional property
const car: {type: string, model: string, year?: number} = {
    type: "Toyota",
    model: "carolla"
    // year: 2009
};

console.log(car);
