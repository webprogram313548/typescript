interface Rectangle {
    height: number,
    width: number
  }

  //atten interface
  interface ColoreRectangle extends Rectangle {
    color: string
  }
  const rectangle: Rectangle = {
    height: 20,
    width: 10
  }

  console.log(rectangle)

  const coloreRectangle: ColoreRectangle = {
    width: 20,
    height: 10,
    color: "red"
  }
  console.log(coloreRectangle)