// enum CardinalDirections {
//     Nort,
//     East,
//     South,
//     West
// }
// let currentDirection = CardinalDirections.Nort;
// console.log(currentDirection);
// currentDirection = 'North'; //not is same type

enum CardinalDirections {
    Nort = "Nort",
    East = "East",
    South = "South",
    West = "West",
}
let currentDirection = CardinalDirections.East;
console.log(currentDirection);